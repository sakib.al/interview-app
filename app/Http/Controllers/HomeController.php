<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        if (Auth::user()->role == 'admin') {

            return view('admin');

        }else{

            return redirect()->back();
        }

    }

    public function readOnly()
    {
        if (Auth::user()->role == 'read_only') {

            return view('readonly');

        }else{

            return redirect()->back();
        }

    }
}
